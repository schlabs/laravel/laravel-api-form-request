
# laravel-api-form-request
  

## Description

Schlabs standard form request errors for api projects
  

## Depencencies

* [laravel-api-response](https://gitlab.com/schlabs/laravel/laravel-api-form-request)
  

## Installation
  

**In your composer.json**

Add the following code to the "repositories" key:

```json
"repositories": [
	{
		"type": "vcs",
		"url": "https://gitlab.com/schlabs/laravel/laravel-api-form-request"
	}
]
```

Add this line to your require dependecies:

```json
"schlabs/laravel-api-form-request": "1.0.*"
```
 
**In your project root run:**
```sh
composer update
```

## Usage

Include the trait "SchLabs\ApiFormRequest\Traits\ApiFormRequest" in your FormRequest class