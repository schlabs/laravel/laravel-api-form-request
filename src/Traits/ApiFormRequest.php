<?php

namespace SchLabs\LaravelApiFormRequest\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use SchLabs\LaravelApiResponse\Traits\ApiResponse;

trait ApiFormRequest {

    use ApiResponse;

    /**
     * @override \Illuminate\Foundation\Http\FormRequest
     * API error response generator
     * For API use
     * Throw form request exceptions as json
     *
     * @param Validator $validator
     * @throws ValidationException
     */
    public function failedValidation(Validator $validator)
    {
        $response = $this->failure($validator->errors(), 422, 422);

        throw new ValidationException($validator, $response);
    }

}